<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2>Flowers</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Name</th>
                        <th>Soil</th>
                        <th>Origin</th>
                        <th>Color of stalk</th>
                        <th>Color of leafs</th>
                        <th>Average size</th>
                        <th>Temperature in degrees</th>
                        <th>Need lighting</th>
                        <th>Watering per week(in ml)</th>
                        <th>Multiplying</th>
                    </tr>
                    <xsl:for-each select="flowers/flower">
                        <xsl:sort select="name"/>
                        <tr>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="soil"/></td>
                            <td><xsl:value-of select="origin"/></td>
                            <td><xsl:value-of select="visualParameters/stalkColor"/></td>
                            <td><xsl:value-of select="visualParameters/leafsColor"/></td>
                            <td><xsl:value-of select="visualParameters/averageSize"/></td>
                            <td><xsl:value-of select="growingTips/temperatureInDegrees"/></td>
                            <td><xsl:value-of select="growingTips/lighting"/></td>
                            <td><xsl:value-of select="growingTips/wateringPerWeek"/></td>
                            <td><xsl:value-of select="propagation"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>