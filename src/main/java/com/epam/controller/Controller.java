package com.epam.controller;

import com.epam.XMLAction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Controller {
    private final Logger LOGGER = LogManager.getLogger(Controller.class);
    private final XMLAction XML_ACTION = new XMLAction();

    public String getParsedByDOM() {
        String parsed = XML_ACTION.parseByDom();
        if (parsed == null) {
            String logMessage = "Error with parsing";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
        }
        return XML_ACTION.parseByDom();
    }

    public String getParsedBySAX() {
        return XML_ACTION.parseBySAX();
    }

    public String getParsedByStAX() {
        return XML_ACTION.parseByStAX();
    }

    public boolean validate() {
        return XML_ACTION.validate();
    }

    public void transformIntoHTMLAndBack() {
        XML_ACTION.transformInfoHTMLAndBack();
    }
}
