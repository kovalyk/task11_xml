package com.epam.parser.stax;

import com.epam.model.*;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StAXReader {

    public static List<Flower> parseBeers(File xml) {
        List<Flower> flowerList = new ArrayList<>();
        Flower flower = null;
        GrowingTips growingTips = null;
        VisualParameters visualParameters = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory
                    .createXMLEventReader(new FileInputStream(xml));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "flower":
                            flower = new Flower();
                            break;
                        case "name":
                            xmlEvent = xmlEventReader.nextEvent();
                            flower.setName(xmlEvent.asCharacters().getData());
                            break;
                        case "soil":
                            xmlEvent = xmlEventReader.nextEvent();
                            flower.setSoil(Soil.valueOf(xmlEvent
                                    .asCharacters().getData().toUpperCase()));
                            break;
                        case "origin":
                            xmlEvent = xmlEventReader.nextEvent();
                            flower.setOrigin(xmlEvent.asCharacters().getData());
                            break;
                        case "visualParameters":
                            xmlEvent = xmlEventReader.nextEvent();
                            visualParameters = new VisualParameters();
                            break;
                        case "stalkColor":
                            xmlEvent = xmlEventReader.nextEvent();
                            visualParameters.setStalkColor(
                                    xmlEvent.asCharacters().getData());
                            break;
                        case "leafsColor":
                            xmlEvent = xmlEventReader.nextEvent();
                            visualParameters.setLeafsColor(
                                    xmlEvent.asCharacters().getData());
                            break;
                        case "averageSize":
                            xmlEvent = xmlEventReader.nextEvent();
                            visualParameters.setAverageSize(Integer.parseInt(
                                    xmlEvent.asCharacters().getData()));
                            break;
                        case "growingTips":
                            xmlEvent = xmlEventReader.nextEvent();
                            growingTips = new GrowingTips();
                            break;
                        case "temperatureInDegrees":
                            xmlEvent = xmlEventReader.nextEvent();
                            growingTips
                                    .setTemperatureInDegrees(Integer.parseInt(
                                            xmlEvent.asCharacters().getData()));
                            break;
                        case "lighting":
                            xmlEvent = xmlEventReader.nextEvent();
                            growingTips.setLighting(Boolean.parseBoolean(
                                    xmlEvent.asCharacters().getData()));
                            break;
                        case "wateringPerWeek":
                            xmlEvent = xmlEventReader.nextEvent();
                            growingTips.setWateringPerWeek(Integer.parseInt(
                                    xmlEvent.asCharacters().getData()));
                            break;
                        case "multiplying":
                            xmlEvent = xmlEventReader.nextEvent();
                            flower.setPropagation(Propagation.valueOf(xmlEvent
                                    .asCharacters().getData().toUpperCase()));
                            flower.setVisualParameter(visualParameters);
                            flower.setGrowingTips(growingTips);
                            break;
                    }
                }
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("flower")) {
                        flowerList.add(flower);
                    }
                }
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return flowerList;
    }
}

