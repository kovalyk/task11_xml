package com.epam.parser.dom;

import com.epam.model.Flower;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class DOMParserUser {
    private static final Logger LOGGER = LogManager
            .getLogger(DOMParserUser.class);

    public static List<Flower> getFlowerList(File xml, File xsd) {
        DOMDocCreator creator = new DOMDocCreator(xml);
        Document doc = creator.getDocument();
        try {
            DOMValidator.validate(DOMValidator.createSchema(xsd), doc);
        } catch (IOException | SAXException ex) {
            String logMessage = "Error with DOM validation";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            LOGGER.fatal(logMessage);
            ex.printStackTrace();
        }
        DOMDocReader reader = new DOMDocReader();
        return reader.readDoc(doc);
    }
}
