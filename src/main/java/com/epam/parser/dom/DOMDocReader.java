package com.epam.parser.dom;

import com.epam.model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMDocReader {

    public List<Flower> readDoc(Document doc) {
        doc.getDocumentElement().normalize();
        List<Flower> flowers = new ArrayList<>();
        NodeList nodeList = doc.getElementsByTagName("flower");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Flower flower = new Flower();
            VisualParameters visualParameters;
            GrowingTips growingTips;
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                flower.setName(element.getElementsByTagName("name").item(0)
                        .getTextContent());
                flower.setSoil(Soil.valueOf(element
                        .getElementsByTagName("soil").item(0)
                        .getTextContent().toUpperCase()));
                flower.setOrigin(element.getElementsByTagName("origin")
                        .item(0).getTextContent());
                flower.setPropagation(Propagation.valueOf(element
                        .getElementsByTagName("multiplying").item(0)
                        .getTextContent().toUpperCase()));
                visualParameters = getVisualParameters(element
                        .getElementsByTagName("visualParameters"));
                growingTips = getGrowingTips(element
                        .getElementsByTagName("growingTips"));
                flower.setVisualParameter(visualParameters);
                flower.setGrowingTips(growingTips);
                flowers.add(flower);
            }
        }
        return flowers;
    }

    private VisualParameters getVisualParameters(NodeList nodes) {
        VisualParameters visualParameters = new VisualParameters();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            visualParameters.setStalkColor(element.getElementsByTagName(
                    "stalkColor").item(0).getTextContent());
            visualParameters.setLeafsColor(element.getElementsByTagName(
                    "leafsColor").item(0).getTextContent());
            try {
                visualParameters.setAverageSize(Integer.parseInt(element
                        .getElementsByTagName("averageSize").item(0)
                        .getTextContent()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return visualParameters;
    }

    private GrowingTips getGrowingTips(NodeList nodes) {
        GrowingTips growingTips = new GrowingTips();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            try {
                growingTips.setTemperatureInDegrees(Integer.parseInt(element.
                        getElementsByTagName("temperatureInDegrees")
                        .item(0).getTextContent()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            growingTips.setLighting(Boolean.parseBoolean(element.
                    getElementsByTagName("lighting").item(0)
                    .getTextContent()));
            try {
                growingTips.setWateringPerWeek(Integer.parseInt(element.
                        getElementsByTagName("wateringPerWeek").item(0)
                        .getTextContent()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return growingTips;
    }
}
