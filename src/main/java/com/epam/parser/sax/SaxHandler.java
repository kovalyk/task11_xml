package com.epam.parser.sax;

import com.epam.model.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.List;
import org.xml.sax.helpers.DefaultHandler;

class SAXHandler extends DefaultHandler {
  private List<Flower> flowers = new ArrayList<>();
  private Flower flower = null;
  private VisualParameters visualParameters = null;
  private GrowingTips growingTips = null;

  private boolean flowerName = false;
  private boolean flowerSoil = false;
  private boolean flowerOrigin = false;
  private boolean flowerStalkColor = false;
  private boolean flowerLeafsColor = false;
  private boolean flowerAverageSize = false;
  private boolean flowerTemperatureInDegrees = false;
  private boolean flowerLighting = false;
  private boolean flowerWateringPerWeek = false;
  private boolean flowerMultiplying = false;

  public List<Flower> getFlowerList() {
    return flowers;
  }

  @Override
  public void startElement(String uri, String localName, String qName,
                           Attributes attributes) throws SAXException {
    if (qName.equalsIgnoreCase("flower")) {
      flower = new Flower();
    } else if (qName.equalsIgnoreCase("name")) {
      flowerName = true;
    } else if (qName.equalsIgnoreCase("soil")) {
      flowerSoil = true;
    } else if (qName.equalsIgnoreCase("origin")) {
      flowerOrigin = true;
    } else if (qName.equalsIgnoreCase("visualParameters")) {
      visualParameters = new VisualParameters();
    } else if (qName.equalsIgnoreCase("stalkColor")) {
      flowerStalkColor = true;
    } else if (qName.equalsIgnoreCase("leafsColor")) {
      flowerLeafsColor = true;
    } else if (qName.equalsIgnoreCase("averageSize")) {
      flowerAverageSize = true;
    } else if (qName.equalsIgnoreCase("growingTips")) {
      growingTips = new GrowingTips();
    } else if (qName.equalsIgnoreCase("temperatureInDegrees")) {
      flowerTemperatureInDegrees = true;
    } else if (qName.equalsIgnoreCase("lighting")) {
      flowerLighting = true;
    } else if (qName.equalsIgnoreCase("wateringPerWeek")) {
      flowerWateringPerWeek = true;
    } else if (qName.equalsIgnoreCase("multiplying")) {
      flowerMultiplying = true;
    }
  }

  @Override
  public void endElement(String uri, String localName, String qName)
          throws SAXException {
    if (qName.equalsIgnoreCase("flower")) {
      flowers.add(flower);
    }
  }

  @Override
  public void characters(char[] ch, int start, int length)
          throws SAXException {
    if (flowerName) {
      flower.setName(new String(ch, start, length));
      flowerName = false;
    } else if (flowerSoil) {
      flower.setSoil(Soil.valueOf(
              new String(ch, start, length).toUpperCase()));
      flowerSoil = false;
    } else if (flowerOrigin) {
      flower.setOrigin(new String(ch, start, length));
      flowerOrigin = false;
    } else if (flowerStalkColor) {
      visualParameters.setStalkColor(new String(ch, start, length));
      flowerStalkColor = false;
    } else if (flowerLeafsColor) {
      visualParameters.setLeafsColor(new String(ch, start, length));
      flowerLeafsColor = false;
    } else if (flowerAverageSize) {
      try {
        visualParameters.setAverageSize(Integer.parseInt(
                new String(ch, start, length)));
      } catch (Exception e) {
        e.printStackTrace();
      }
      flowerAverageSize = false;
    } else if (flowerTemperatureInDegrees) {
      try {
        growingTips.setTemperatureInDegrees(Integer.parseInt(
                new String(ch, start, length)));
      } catch (Exception e) {
        e.printStackTrace();
      }
      flowerTemperatureInDegrees = false;
    } else if (flowerLighting) {
      growingTips.setLighting(Boolean.parseBoolean(
              new String(ch, start, length)));
      flowerLighting = false;
    } else if (flowerWateringPerWeek) {
      try {
        growingTips.setWateringPerWeek(Integer.parseInt(
                new String(ch, start, length)));
      } catch (Exception e) {
        e.printStackTrace();
      }
      flowerWateringPerWeek = false;
    } else if (flowerMultiplying) {
      flower.setPropagation(Propagation.valueOf(
              new String(ch, start, length).toUpperCase()));
      flowerMultiplying = false;
      flower.setVisualParameter(visualParameters);
      flower.setGrowingTips(growingTips);
    }
  }
}

