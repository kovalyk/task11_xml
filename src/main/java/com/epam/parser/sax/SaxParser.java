//package com.epam.parser.sax;
//
//import com.epam.model.Flower;
//import com.epam.parser.dom.XmlValidator;
//import org.xml.sax.SAXException;
//
//import javax.xml.parsers.ParserConfigurationException;
//import javax.xml.parsers.SAXParser;
//import javax.xml.parsers.SAXParserFactory;
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//
//public class SaxParser {
//
//  private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
//
//  public static List<Flower> parse(File xml, File xsd) {
//    List<Flower> flowers = new ArrayList<>();
//    try {
//      saxParserFactory.setSchema(XmlValidator.createSchema(xsd));
//      saxParserFactory.setValidating(true);
//
//      SAXParser saxParser = saxParserFactory.newSAXParser();
//      System.out.println(saxParser.isValidating());
//      SaxHandler saxHandler = new SaxHandler();
//      saxParser.parse(xml, saxHandler);
//      flowers = saxHandler.getFlowerList();
//    } catch (SAXException | ParserConfigurationException | IOException ex) {
//      ex.printStackTrace();
//    }
//    return flowers;
//  }
//}