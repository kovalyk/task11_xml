package com.epam.view;

import com.epam.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;

public abstract class AbstractMenu {
    protected Map<Integer, String> menu = new HashMap<>();
    protected Map<Integer, Printable> methodsForMenu = new HashMap<>();
    protected final Controller CONTROLLER = new Controller();
    protected Scanner scan = new Scanner(System.in, "UTF-8");
    protected final Logger LOGGER = LogManager.getLogger(AbstractMenu.class);

    protected void out() {
        int inputted = 0;
        Optional<Printable> opt;
        do {
            showInfo();
            scan = new Scanner(System.in, "UTF-8");
            if (scan.hasNextInt()) {
                inputted = scan.nextInt();
                opt = Optional.ofNullable(methodsForMenu.get(inputted));
                opt.orElse(this::badInput)
                        .print();
            } else {
                badInput();
            }
        } while (inputted != menu.size() - 1);
    }

    protected void quit() {
        LOGGER.info("Bye Bye");
    }

    protected void showInfo() {
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }

    protected void badInput() {
        LOGGER.error("You entered bad value");
    }
}

