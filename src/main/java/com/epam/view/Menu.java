package com.epam.view;

public final class Menu extends AbstractMenu {

    public Menu() {
        menu.put(0, "Parse by DOM");
        menu.put(1, "Parse by SAX");
        menu.put(2, "Parse by STAX");
        menu.put(3, "Validate");
        menu.put(4, "Transform into HTML and back");
        menu.put(5, "Exit");
        methodsForMenu.put(0, this::printByDOMParsed);
        methodsForMenu.put(1, this::printBySAXParsed);
        methodsForMenu.put(2, this::printByStAXParsed);
        methodsForMenu.put(3, this::printWhetherIsValidated);
        methodsForMenu.put(4, this::transformIntoHTMLAndBack);
        methodsForMenu.put(5, this::quit);
    }

    public void show() {
        out();
    }

    @Override
    protected void showInfo() {
        LOGGER.info("Menu for task13_XML");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }

    private void printByDOMParsed() {
        LOGGER.info(CONTROLLER.getParsedByDOM());
    }

    private void printBySAXParsed() {
        LOGGER.info(CONTROLLER.getParsedBySAX());
    }

    private void printByStAXParsed() {
        LOGGER.info(CONTROLLER.getParsedByStAX());
    }

    private void printWhetherIsValidated() {
        boolean isValidated = CONTROLLER.validate();
        if (!isValidated) {
            String logMessage = "Is not validated " +
                    "(must be false for proving that it works)";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            LOGGER.fatal(logMessage);
        }
        LOGGER.info("Is validated : " + isValidated
                + " (must be false for proving that it works)");
    }

    private void transformIntoHTMLAndBack() {
        LOGGER.info("Transforming...");
        CONTROLLER.transformIntoHTMLAndBack();
        LOGGER.info("Result is in project folder");
    }
}

