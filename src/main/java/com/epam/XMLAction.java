package com.epam;

import com.epam.model.Flower;
import com.epam.model.FlowerComparator;
import com.epam.parser.ExtensionChecker;
import com.epam.parser.dom.DOMDocCreator;
import com.epam.parser.dom.DOMParserUser;
import com.epam.parser.dom.DOMValidator;
import com.epam.parser.sax.SAXParserUser;
import com.epam.parser.stax.StAXReader;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.Collections;
import java.util.List;

public class XMLAction {
    private final File xml = new File("src\\main\\resources\\xml\\orangery" +
            ".xml");
    private final File xsd = new File("src\\main\\resources\\xml\\orangery" +
            ".xsd");
    private final File xsl = new File("src\\main\\resources\\xml\\orangery" +
            ".xsl");
    private final String outputHTMLFileName = "orangery.html";
    private final String outputXMLFileName = "orangery1.xml";

    public String parseByDom() {
        if (checkIfXML(xml) && checkIfXSD(xsd)) {
            return getParsedXML(DOMParserUser.getFlowerList(xml, xsd),
                    "DOM");
        }
        return null;
    }

    public String parseBySAX() {
        if (checkIfXML(xml) && checkIfXSD(xsd)) {
            return getParsedXML(SAXParserUser.parseFlowers(xml, xsd),
                    "SAX");
        }
        return null;
    }

    public String parseByStAX() {
        if (checkIfXML(xml) && checkIfXSD(xsd)) {
            return getParsedXML(StAXReader.parseBeers(xml),
                    "StAX");
        }
        return null;
    }

    public boolean validate() {
        if (checkIfXML(xml) && checkIfXSD(xsd)) {
            DOMDocCreator creator = new DOMDocCreator(xml);
            Document doc = creator.getDocument();
            try {
                DOMValidator.validate(DOMValidator.createSchema(xsd), doc);
            } catch (IOException | SAXException ex) {
                return false;
            }
            return true;
        }
        return false;
    }

    public void transformInfoHTMLAndBack() {
        TransformerFactory tFactory = TransformerFactory.newInstance();
        try {
            transformIntoHTML(tFactory);
            transformIntoXML(tFactory);
        } catch (FileNotFoundException | TransformerException e) {
            e.printStackTrace();
        }
    }

    private void transformIntoHTML(TransformerFactory tFactory)
            throws FileNotFoundException, TransformerException {
        Source xslDoc = new StreamSource(xsl);
        Source xmlDoc = new StreamSource(xml);
        OutputStream htmlFile;
        htmlFile = new FileOutputStream(outputHTMLFileName);
        Transformer transform = tFactory.newTransformer(xslDoc);
        transform.transform(xmlDoc, new StreamResult(htmlFile));
    }

    private void transformIntoXML(TransformerFactory tFactory)
            throws TransformerException {
        DOMDocCreator creator = new DOMDocCreator(xml);
        Document doc = creator.getDocument();
        Node root = doc.getElementsByTagName("flowers").item(0);
        root.setTextContent("Changed root element");
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File(outputXMLFileName));
        Transformer transformer = tFactory.newTransformer();
        transformer.transform(source, result);
    }

    private boolean checkIfXSD(File xsd) {
        return ExtensionChecker.isXSD(xsd);
    }

    private boolean checkIfXML(File xml) {
        return ExtensionChecker.isXML(xml);
    }

    private String getParsedXML(List<Flower> flowers, String parserName) {
        Collections.sort(flowers, new FlowerComparator());
        StringBuilder parsed = new StringBuilder();
        parsed.append(parserName);
        parsed.append("\n");
        flowers.forEach(s -> parsed.append(s).append("\n"));
        return parsed.toString();
    }
}

