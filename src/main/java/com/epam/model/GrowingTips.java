package com.epam.model;

import java.util.Objects;

public class GrowingTips {
    private int temperatureInDegrees;
    private boolean lighting;
    private int wateringPerWeek;

    public GrowingTips() {
    }

    public GrowingTips(int temperatureInDegrees, boolean lighting,
                       int wateringPerWeek) {
        this.temperatureInDegrees = temperatureInDegrees;
        this.lighting = lighting;
        this.wateringPerWeek = wateringPerWeek;
    }

    public int getTemperatureInDegrees() {
        return temperatureInDegrees;
    }

    public void setTemperatureInDegrees(int temperatureInDegrees) {
        this.temperatureInDegrees = temperatureInDegrees;
    }

    public boolean isLighting() {
        return lighting;
    }

    public void setLighting(boolean lighting) {
        this.lighting = lighting;
    }

    public int getWateringPerWeek() {
        return wateringPerWeek;
    }

    public void setWateringPerWeek(int wateringPerWeek) {
        this.wateringPerWeek = wateringPerWeek;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GrowingTips that = (GrowingTips) o;
        return temperatureInDegrees == that.temperatureInDegrees &&
                lighting == that.lighting &&
                wateringPerWeek == that.wateringPerWeek;
    }

    @Override
    public int hashCode() {
        return Objects.hash(temperatureInDegrees, lighting, wateringPerWeek);
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperatureInDegrees=" + temperatureInDegrees +
                ", lighting=" + lighting +
                ", wateringPerWeek=" + wateringPerWeek +
                '}';
    }

    private boolean checkTemperatureInDegrees(int temperatureInDegrees) {
        return temperatureInDegrees >= -99 && temperatureInDegrees <= 99;
    }

    private boolean checkWateringPerWeek(int wateringPerWeek) {
        return wateringPerWeek >= 0 && wateringPerWeek <= 999;
    }
}

//    private int temperature;
//    private Soil lightIntensity;
//    private int watering;
//
//    public GrowingTips() {
//    }
//
//    public GrowingTips(int temperature, Soil lightIntensity, int watering) {
//        this.temperature = temperature;
//        this.lightIntensity = lightIntensity;
//        this.watering = watering;
//    }
//
//    public int getTemperature() {
//        return temperature;
//    }
//
//    public void setTemperature(int temperature) {
//        this.temperature = temperature;
//    }
//
//    public Soil getLightIntensity() {
//        return lightIntensity;
//    }
//
//    @Override
//    public Soil toString() {
//        return "GrowingTips{" +
//                "temperature=" + temperature +
//                ", lightIntensity='" + lightIntensity + '\'' +
//                ", watering=" + watering +
//                '}';
//    }
//
//    public void setLightIntensity(Soil lightIntensity) {
//        this.lightIntensity = lightIntensity;
//    }
//
//    public int getWatering() {
//        return watering;
//    }
//
//    public void setWatering(int watering) {
//        this.watering = watering;
//    }
//}
