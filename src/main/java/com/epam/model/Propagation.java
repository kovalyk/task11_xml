package com.epam.model;

public enum Propagation {
    LEAF, STEM, SEED
}

