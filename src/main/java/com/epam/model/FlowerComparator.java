package com.epam.model;

import java.util.Comparator;

public class FlowerComparator implements Comparator<Flower> {
    @Override
    public int compare(Flower flower, Flower flower1) {
        return flower.getName().compareTo(flower1.getName());
    }
}
