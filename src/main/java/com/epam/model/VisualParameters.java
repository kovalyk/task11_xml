package com.epam.model;

import java.util.Objects;

public class VisualParameters {
    private String stalkColor;
    private String leafsColor;
    private int averageSize;

    public VisualParameters() {
    }

    public VisualParameters(String stalkColor, String leafsColor,
                            int averageSize) {
        this.stalkColor = stalkColor;
        this.leafsColor = leafsColor;
        this.averageSize = averageSize;
    }

    public String getStalkColor() {
        return stalkColor;
    }

    public void setStalkColor(String stalkColor) {
        this.stalkColor = stalkColor;
    }

    public String getLeafsColor() {
        return leafsColor;
    }

    public void setLeafsColor(String leafsColor) {
        this.leafsColor = leafsColor;
    }

    public int getAverageSize() {
        return averageSize;
    }

    public void setAverageSize(int averageSize) {
        this.averageSize = averageSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VisualParameters that = (VisualParameters) o;
        return averageSize == that.averageSize &&
                Objects.equals(stalkColor, that.stalkColor) &&
                Objects.equals(leafsColor, that.leafsColor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stalkColor, leafsColor, averageSize);
    }

    @Override
    public String toString() {
        return "VisualParameter{" +
                "stalkColor='" + stalkColor + '\'' +
                ", leafsColor='" + leafsColor + '\'' +
                ", averageSize=" + averageSize +
                '}';
    }
}
